<?php

namespace Tests\Feature;

use App\Jobs\DownloadVideo;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\DB;
use ReflectionClass;
use Tests\TestCase;

class DownloadVideoJobTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /** @test */
    public function ensure_the_video_download_job_is_dispatched()
    {
        Bus::fake();

        $url        = $this->faker->url;
        $response   = $this->post(route('prepare'), compact('url'));
        $video      = DB::table('videos')->where('url', $url)->first();

        $response->assertRedirect(route('status', ['video' => $video->id]));
        $response->assertSessionHasNoErrors();

        Bus::assertDispatched(DownloadVideo::class, function($job) use ($video) {
            return $this->getPrivateProperty($job, 'video')->id === $video->id;
        });
    }

    protected function getPrivateProperty(object $obj, string $property)
    {
        $reflection = new ReflectionClass($obj);
        $privateProperty = $reflection->getProperty($property);
        $privateProperty->setAccessible(true);

        return $privateProperty->getValue($obj);
    }
}