@extends('layouts.app')

@section('content')
<div class="container">
    <form method="post" action="{{ route('colas') }}">
        @csrf

        @if(Session::has('error'))
            <div class="alert alert-danger">{{ Session::get('error') }}</div>
        @endif
       
        <h1>Colas de los videos</h1>
    
    <table class="table col-12" >
        <thead>
            <tr>
                <td>URL</td>
                <td>Estado</td>
            </tr>
        </thead>
        <tbody>
    @foreach($colas as $cola)
    
        <td>{{ $cola->url }}</td>
        <td>{{ $cola->status }}</td>
        <br>
        </tbody>
    @endforeach
    </table>
    </form>
</div>
@endsection