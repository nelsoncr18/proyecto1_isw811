@extends('layouts.app')

@section('content')
    @if ($video->status == 'Completado')
        <h3>{{ $video->info->title }}</h3>
        <img src="{{ $video->info->thumbnail }}">
        <h3>Click <a href="{{ route('download', [$video]) }}">aqui</a> para descargarlo</h3>
    @endif

    @if($video->status == 'En_progreso')
        <h3>Descarga en progreso..</h3>
        <p>Por favor <a href="javascript:;" onclick="window.reload()">refrescar</a> esta página en algunos segundos.</p>
    @endif

    @if ($video->status == 'Fallido')
        <h3>Descarga fallida!</h3>
        <p>Por favor intentalo de nuevo.</p>
    @endif

@endsection