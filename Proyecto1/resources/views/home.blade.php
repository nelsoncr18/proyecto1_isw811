@extends('layouts.app')

@section('content')
<div class="container">
<div class="row">
    <div class="col-sm"></div>
    <div class="col-sm">
    <form method="post" action="{{ route('prepare') }}">
        @csrf

        @if(Session::has('error'))
            <div class="alert alert-danger">{{ Session::get('error') }}</div>
        @endif

        <div class="form-group">
            <input name="url" type="text" required class="form-control @error('url')  is-invalid @enderror" id="url"
                   aria-describedby="url" value="{{ old('url') }}"
                   autocomplete="off" autofocus>

            @error('url')
                <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>

        <div class="text-center">
            <button class="btn btn-lg btn-primary">Descargar</button>
        </div>
    </form>
    </div>
    <div class="col-sm"></div>
  </div>
</div>
@endsection