#!/usr/bin/env node
var amqp = require('amqplib/callback_api');

//amqp.cone

amqp.connect('amqps://fvlganod:IcRJ_KnFPv7jSGmWrzkE6-kO_fnLPNHP@woodpecker.rmq.cloudamqp.com/fvlganod', function(err, conn){
    conn.createChannel(function(err, ch) {
        var q = 'task_queue';

        ch.assertQueue(q, {durable: true});
        ch.prefetch(1);
        console.log("[*] Waiting for messages in %s. To exit press CTRL+C", q);
        ch.consume(q, function(msg){
            var secs = msg.content.toString().split('.').length - 1;

            console.log("[x] Received '%s'", msg.content.toString());
            setTimeout(function() {
                console.log("[x] Done");
                ch.ack(msg);
            },secs * 1000);
        }, {noAck: false});
    });
});